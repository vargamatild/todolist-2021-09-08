﻿using System;
using ToDoListFrontEnd.Services;
using ToDoListFrontEnd.View;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoListFrontEnd
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            DependencyService.Register<ToDoItemDataStore>();
            MainPage = new NavigationPage(new ToDoListMainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
