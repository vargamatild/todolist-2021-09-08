﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoListFrontEnd.Services;
using Xamarin.Forms;

namespace ToDoListFrontEnd.ViewModel
{
    public class ToDoItemDetailsViewModel : BaseViewModel
    {
        public ToDoItem model;
        public ToDoItemDataStore ToDoItemDataStore = DependencyService.Get<ToDoItemDataStore>();
        private string description;

        public ToDoItemDetailsViewModel(ToDoItem model)
        {
            this.model = model;
            description = model.Description;
            selectedState = model.State;
        }

        private ItemState selectedState;
        public ItemState State
        {
            get { return selectedState; }
            set
            {
                selectedState = value;
                OnPropertyChanged(nameof(State));
            }
        }

        public List<string> StateNames
        {
            get
            {
                return Enum.GetNames(typeof(ItemState)).ToList();
            }
        }

        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public string CreatedAt => model.CreatedAt.ToString("yyyy. MM d, HH:mm", CultureInfo.InvariantCulture);

        public async Task UpdateItem()
        {
            if (description == model.Description && selectedState == model.State)
                return;

            await ToDoItemDataStore.UpdateItem(description, selectedState, model.Id);
        }

    }
}