﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoListFrontEnd.Model;

namespace ToDoListFrontEnd.ViewModel
{
    public class ToDoListViewModel : BaseViewModel
    {
        private ToDoListModel _toDoListModel;

        public List<ToDoItem> ToDoList
        {
            get => _toDoListModel.ToDoList;
            set { }
        }

        public ToDoListViewModel()
        {
            _toDoListModel = new ToDoListModel();

        }

        public async Task RefreshToDoList()
        {
            await _toDoListModel.RefreshToDoList();
            OnPropertyChanged(nameof(ToDoList));
        }
    }
}
