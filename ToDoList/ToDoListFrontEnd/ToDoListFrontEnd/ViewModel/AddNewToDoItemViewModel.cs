﻿using System.Threading.Tasks;
using ToDoListFrontEnd.Model;

namespace ToDoListFrontEnd.ViewModel
{
    public class AddNewToDoItemViewModel : BaseViewModel
    {
        private AddNewToDoItemModel _addNewToDoItemModel = new AddNewToDoItemModel();

        public string Description
        {
            get
            {
                return _addNewToDoItemModel.Description;
            }
            set
            {
                _addNewToDoItemModel.Description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public async Task AddNewToDoItem()
        {
            await _addNewToDoItemModel.AddNewToDoItem();
        }
    }
}