﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoListFrontEnd.Services;
using Xamarin.Forms;

namespace ToDoListFrontEnd.Model
{
    public class ToDoListModel
    {
        public List<ToDoItem> ToDoList;
        public ToDoItemDataStore ToDoItemDataStore = DependencyService.Get<ToDoItemDataStore>();

        public ToDoListModel()
        {
            ToDoList = new List<ToDoItem>();
        }

        public async Task RefreshToDoList()
        {
            ToDoList = new List<ToDoItem>(await ToDoItemDataStore.GetItemsAsync());
        }
    }
}