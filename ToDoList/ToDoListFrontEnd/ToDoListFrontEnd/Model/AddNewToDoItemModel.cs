﻿using System.Threading.Tasks;
using ToDoListFrontEnd.Services;
using Xamarin.Forms;

namespace ToDoListFrontEnd.Model
{
    public class AddNewToDoItemModel
    {
        public ToDoItemDataStore ToDoItemDataStore = DependencyService.Get<ToDoItemDataStore>();

        public string Description;

        public async Task AddNewToDoItem()
        {
            if (string.IsNullOrEmpty(Description))
                return;

            await ToDoItemDataStore.AddNewToDoItem(Description);
        }
    }
}