﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoListFrontEnd.Services
{
    public interface IDataStore<T>
    {
        Task<T> GetItemAsync(int id, bool forceRefresh = false);

        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}