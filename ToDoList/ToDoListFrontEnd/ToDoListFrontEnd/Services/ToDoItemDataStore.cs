﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ToDoList.Model;
using ToDoList.Model.DTO;

namespace ToDoListFrontEnd.Services
{
    public class ToDoItemDataStore :IDataStore<ToDoItem>
    {
        private static string BaseUrl = "http://10.0.2.2:7071/api";
        private static readonly HttpClient client = new HttpClient(new HttpClientHandler());
        private List<ToDoItem> _toDoItemsList;

        public ToDoItemDataStore()
        {
            _toDoItemsList = new List<ToDoItem>();
        }

        private async Task RefreshToDoItem(int id)
        {
            var index = _toDoItemsList.FindIndex(item => item.Id == id);
            var json = await client.GetAsync(BaseUrl + "/items/" + id).Result.Content.ReadAsStringAsync();
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            _toDoItemsList[index] = JsonConvert.DeserializeObject<ToDoItem>(json, settings);
        }

        private async Task RefreshToDoItemList()
        {
            var json = await client.GetAsync(BaseUrl + "/items").Result.Content.ReadAsStringAsync();
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            _toDoItemsList = JsonConvert.DeserializeObject<List<ToDoItem>>(json, settings);
        }

        public async Task<ToDoItem> GetItemAsync(int id, bool forceRefresh = false)
        {
            if (_toDoItemsList.Count == 0 || forceRefresh) 
                await RefreshToDoItem(id);

            return _toDoItemsList.Find(item => item.Id == id);
        }

        public async Task<IEnumerable<ToDoItem>> GetItemsAsync(bool forceRefresh = false)
        {
            if (_toDoItemsList.Count == 0 || forceRefresh) 
                await RefreshToDoItemList();

            return _toDoItemsList;
        }

        public async Task AddNewToDoItem(string description)
        {
            try
            {
                AddItemDTO addItemDto = new AddItemDTO()
                {
                    Description = description
                };

                var json = JsonConvert.SerializeObject(addItemDto);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await client.PostAsync(BaseUrl + "/items", content);
                await GetItemsAsync(true);
            }
            catch
            {
                // nothing to do
            }
        }

        public async Task UpdateItem(string description, ItemState state, int id)
        {
            try
            {
                UpdateItemDTO updateDTO = new UpdateItemDTO()
                {
                    NewDescription = description,
                    NewState = state
                };
                var json = JsonConvert.SerializeObject(updateDTO);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("PATCH"), BaseUrl + "/items/" + id);
                request.Content = content;
                var response = await client.SendAsync(request);
                await GetItemAsync(id, true);
            }
            catch
            {
                // nothing to do
            }
        }

    }
}