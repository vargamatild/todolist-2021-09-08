﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoListFrontEnd.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ToDoItemCardView : ViewCell
    {
        public ToDoItemCardView()
        {
            InitializeComponent();
        }
    }
}