﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoListFrontEnd.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoListFrontEnd.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ToDoItemDetailsPage : ContentPage
    {
        public ToDoItemDetailsViewModel ToDoItemDetailsViewModel;
        public ToDoItemDetailsPage(ToDoItem toDoItem)
        {
            InitializeComponent();
            ToDoItemDetailsViewModel = new ToDoItemDetailsViewModel(toDoItem);
            BindingContext = ToDoItemDetailsViewModel;
        }
        private async void SaveButtonClicked(object sender, EventArgs e)
        {
            await ToDoItemDetailsViewModel.UpdateItem();
            await Navigation.PopAsync();
        }
    }
}