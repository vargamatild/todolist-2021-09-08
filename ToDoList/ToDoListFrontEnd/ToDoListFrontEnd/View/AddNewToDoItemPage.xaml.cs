﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListFrontEnd.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoListFrontEnd.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNewToDoItemPage : ContentPage
    {
        public AddNewToDoItemViewModel AddNewToDoItemViewModel;

        public AddNewToDoItemPage()
        {
            InitializeComponent();
            AddNewToDoItemViewModel = new AddNewToDoItemViewModel();
            BindingContext = AddNewToDoItemViewModel;
        }

        private async void SaveNewButtonClicked(object sender, EventArgs e)
        {
            await AddNewToDoItemViewModel.AddNewToDoItem();
            await Navigation.PopAsync();
        }

        private async void BackButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}