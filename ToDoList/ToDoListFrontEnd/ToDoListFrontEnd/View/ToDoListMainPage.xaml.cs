﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoListFrontEnd.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoListFrontEnd.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ToDoListMainPage : ContentPage
    {
        public ToDoListViewModel ToDoListViewModel { get; set; }

        public ToDoListMainPage()
        {
            InitializeComponent();
            ToDoListViewModel = new ToDoListViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Task.Run(() => ToDoListViewModel.RefreshToDoList());
            BindingContext = ToDoListViewModel;
        }

        async void HandleSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var selectedItem = (ToDoItem)e.SelectedItem;

            await Navigation.PushAsync(new ToDoItemDetailsPage(selectedItem));

            ((ListView)sender).SelectedItem = null;
        }

        private async void AddNewToDoItem(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddNewToDoItemPage());
        }
    }
}