﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Model
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        public int ToDoItemId { get; set; }

        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}