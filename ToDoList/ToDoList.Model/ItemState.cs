﻿namespace ToDoList.Model
{
    /// <summary>
    /// Possible states of a todo item.
    /// </summary>
    public enum ItemState
    {
        New,
        Doing,
        Done
    }
}
