﻿namespace ToDoList.Model.DTO
{
    public class AddItemDTO
    {
        public string Description { get; set; }
    }
}