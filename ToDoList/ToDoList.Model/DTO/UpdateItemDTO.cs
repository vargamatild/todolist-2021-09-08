﻿namespace ToDoList.Model.DTO
{
    public class UpdateItemDTO
    {
        public string NewDescription { get; set; }

        public ItemState NewState { get; set; }
    }
}