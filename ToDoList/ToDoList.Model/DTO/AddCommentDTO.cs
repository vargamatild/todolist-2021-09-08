﻿namespace ToDoList.Model.DTO
{
    public class AddCommentDTO
    {
        public string Text { get; set; }
    }
}