﻿namespace ToDoList.Model.DTO
{
    public class UpdateCommentDTO
    {
        public string NewText { get; set; }
    }
}