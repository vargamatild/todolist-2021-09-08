﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Model
{
    public class ToDoItem
    {
        [Key]
        public int Id { get; set; }
        
        public string Description { get; set; }

        public ItemState State { get; set; }

        public DateTime CreatedAt { get; set; }

        public List<Comment> Comments { get; set; }
    }
}