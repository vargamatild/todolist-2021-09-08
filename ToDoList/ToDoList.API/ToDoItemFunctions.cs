using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ToDoList.BLL;
using ToDoList.Model;
using ToDoList.Model.DTO;

namespace ToDoList.API
{
    public class ToDoItemFunctions
    {
        private ToDoListDbContext _dbContext;
        private readonly ToDoItemManager _itemManager;

        public ToDoItemFunctions(ToDoListDbContext context)
        {
            _dbContext = context;
            _itemManager = new ToDoItemManager(context);
        }

        // Route=null -> localhost:5000/api/AddItem
        // Route="item" -> localhost:5000/api/item
        [FunctionName("AddItem")]
        public async Task<IActionResult> AddItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "items")] HttpRequest req,
            ILogger log)
        {
            try
            {
                AddItemDTO addItemDto = null;

                try
                {
                    string requestBody;
                    using (var bodyReader = new StreamReader(req.Body))
                        requestBody = await bodyReader.ReadToEndAsync();

                    addItemDto = JsonConvert.DeserializeObject<AddItemDTO>(requestBody);
                }
                catch (Exception e)
                {
                    var result = new ObjectResult(e);
                    result.StatusCode = StatusCodes.Status400BadRequest;
                    return result;
                }

                var newItem = _itemManager.AddItemAsync(addItemDto);
                return new OkObjectResult(newItem);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        [FunctionName("GetItem")]
        public async Task<IActionResult> GetItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "items/{id}")] HttpRequest req,
            int id,
            ILogger log)
        {
            try
            {
                ToDoItem requestedItem = await _itemManager.GetItemAsync(id);

                if (requestedItem == null)
                {
                    return new NotFoundResult();
                }

                return new OkObjectResult(requestedItem);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        [FunctionName("GetItems")]
        public async Task<IActionResult> GetItems(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "items")] HttpRequest req,
            ILogger log)
        {
            try
            {
                List<ToDoItem> requestedItems = await _itemManager.GetAllItemsAsync();

                if (requestedItems == null)
                {
                    return new NotFoundResult();
                }

                return new OkObjectResult(requestedItems);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        [FunctionName("UpdateItem")]
        public async Task<IActionResult> UpdateItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "items/{id}")] HttpRequest req,
            int id,
            ILogger log)
        {
            try
            {
                UpdateItemDTO updateItemDto = null;

                try
                {
                    string requestBody;
                    using (var bodyReader = new StreamReader(req.Body))
                        requestBody = await bodyReader.ReadToEndAsync();

                    updateItemDto = JsonConvert.DeserializeObject<UpdateItemDTO>(requestBody);
                }
                catch (Exception e)
                {
                    var result = new ObjectResult(e);
                    result.StatusCode = StatusCodes.Status400BadRequest;
                    return result;
                }

                var updatedItem = await _itemManager.UpdateItemAsync(id, updateItemDto);
                return new OkObjectResult(updatedItem);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }


        [FunctionName("AddComment")]
        public async Task<IActionResult> AddComment(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "items/{id}/comments")] HttpRequest req,
            int id,
            ILogger log)
        {
            try
            {
                AddCommentDTO addCommentDto = null;

                try
                {
                    string requestBody;
                    using (var bodyReader = new StreamReader(req.Body))
                        requestBody = await bodyReader.ReadToEndAsync();

                    addCommentDto = JsonConvert.DeserializeObject<AddCommentDTO>(requestBody);
                }
                catch (Exception e)
                {
                    var result = new ObjectResult(e);
                    result.StatusCode = StatusCodes.Status400BadRequest;
                    return result;
                }

                var newItem = await _itemManager.AddCommentAsync(id, addCommentDto);
                return new OkObjectResult(newItem);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        [FunctionName("UpdateComment")]
        public async Task<IActionResult> UpdateComment(
            [HttpTrigger(AuthorizationLevel.Anonymous, "patch", Route = "items/{itemId}/comments/{commentId}")] HttpRequest req,
            int commentId,
            ILogger log)
        {
            try
            {
                UpdateCommentDTO updateCommentDto = null;

                try
                {
                    string requestBody;
                    using (var bodyReader = new StreamReader(req.Body))
                        requestBody = await bodyReader.ReadToEndAsync();

                    updateCommentDto = JsonConvert.DeserializeObject<UpdateCommentDTO>(requestBody);
                }
                catch (Exception e)
                {
                    var result = new ObjectResult(e);
                    result.StatusCode = StatusCodes.Status400BadRequest;
                    return result;
                }

                var newItem = await _itemManager.UpdateCommentAsync(commentId, updateCommentDto);
                return new OkObjectResult(newItem);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        [FunctionName("GetComments")]
        public async Task<IActionResult> GetComments(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "items/{id}/comments")] HttpRequest req,
            int id,
            ILogger log)
        {
            try
            {
                List<Comment> requestedComments = await _itemManager.GetItemCommentsAsync(id);

                if (requestedComments == null)
                {
                    return new NotFoundResult();
                }

                return new OkObjectResult(requestedComments);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        [FunctionName("GetComment")]
        public async Task<IActionResult> GetComment(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "items/{itemId}/comments/{commentId}")] HttpRequest req,
            int itemId,
            int commentId,
            ILogger log)
        {
            try
            {
                Comment requestedComment = await _itemManager.GetItemSpecificCommentAsync(itemId, commentId);

                if (requestedComment == null)
                {
                    return new NotFoundResult();
                }

                return new OkObjectResult(requestedComment);
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

    }
}
