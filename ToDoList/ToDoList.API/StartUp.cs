﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

[assembly: FunctionsStartup(typeof(ToDoList.API.StartUp))]
namespace ToDoList.API
{
    class StartUp : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            var connectionString = Environment.GetEnvironmentVariable("ToDoListDbConnection");

            if (connectionString == null)
            {
                connectionString = "Data Source=DESKTOP-4LSR426\\SQLEXPRESS;Initial Catalog=ToDoItemsDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            }

            builder.Services.AddDbContext<ToDoList.BLL.ToDoListDbContext>(
                options => options.UseSqlServer(connectionString, options => options.EnableRetryOnFailure())
                );
        }
    }
}
