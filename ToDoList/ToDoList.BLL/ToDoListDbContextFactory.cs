﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace ToDoList.BLL
{
    public class ToDoListDbContextFactory : IDesignTimeDbContextFactory<ToDoListDbContext>
    {
        public ToDoListDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ToDoListDbContext>();

            var connectionString = Environment.GetEnvironmentVariable("ToDoListDbConnection");

            if (connectionString != null)
                optionsBuilder = optionsBuilder.UseSqlServer(connectionString,
                    options => options.EnableRetryOnFailure());

            return new ToDoListDbContext(optionsBuilder.Options);
        }
    }
}
