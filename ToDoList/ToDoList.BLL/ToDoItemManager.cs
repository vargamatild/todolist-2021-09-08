﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToDoList.Model;
using ToDoList.Model.DTO;

namespace ToDoList.BLL
{
    public class ToDoItemManager
    {
        private readonly ToDoListDbContext _context;

        public ToDoItemManager(ToDoListDbContext context)
        {
            _context = context;

        }

        public async Task<ToDoItem> AddItemAsync(AddItemDTO newItemDto)
        {
            var newItem = new ToDoItem()
            {
                Description = newItemDto.Description,
                State = ItemState.New,
                CreatedAt = DateTime.Now
            };

            _context.Add(newItem);
            await _context.SaveChangesAsync();

            return newItem;
        }

        /*public async Task<ToDoItem> GetItemAsync(int id)
        {
            return await _context.ToDoItemsData.FindAsync(id);
        }*/

        public async Task<ToDoItem> GetItemAsync(int id)
        {
            return await Task.Run(() => _context.ToDoItemsData.Where(x => x.Id == id).Include(x => x.Comments).First());
        }

        public async Task<List<ToDoItem>> GetAllItemsAsync()
        {
            return await _context.ToDoItemsData.Include(x => x.Comments).ToListAsync();
        }

        public async Task<ToDoItem> UpdateItemAsync(int id, UpdateItemDTO updateItemDto)
        {
            var itemToUpdate = await GetItemAsync(id);

            if (itemToUpdate == null)
            {
                return null;
            }

            itemToUpdate.Description = updateItemDto.NewDescription;
            itemToUpdate.State = updateItemDto.NewState;

            await _context.SaveChangesAsync();

            return itemToUpdate;
        }

        public async Task<Comment> AddCommentAsync(int id, AddCommentDTO addCommentDto)
        {
            var itemWhoseAreTheComments = await GetItemAsync(id);

            var newComment = new Comment()
            {
                ToDoItemId = id,
                Text = addCommentDto.Text,
                CreatedAt = DateTime.Now
            };

            _context.Add(newComment);
            itemWhoseAreTheComments.Comments.Add(newComment);
            await _context.SaveChangesAsync();

            return newComment;
        }

        public async Task<List<Comment>> GetItemCommentsAsync(int id)
        {
            var itemWhoseAreTheComments = await GetItemAsync(id);

            return itemWhoseAreTheComments.Comments;
        }

        public async Task<Comment> GetCommentAsync(int id)
        {
            return await _context.ToDoItemsDataComments.FindAsync(id);
        }

        public async Task<Comment> GetItemSpecificCommentAsync(int itemId, int commentId)
        {
            return await Task.Run(() => _context.ToDoItemsData.Where(x => x.Id == itemId).Include(x => x.Comments).First().Comments.First(x => x.Id == commentId));
        }

        public async Task<object> UpdateCommentAsync(int commentId, UpdateCommentDTO updateCommentDto)
        {
            var commentToUpdate = await GetCommentAsync(commentId);

            if (commentToUpdate == null)
            {
                return null;
            }

            commentToUpdate.Text = updateCommentDto.NewText;

            await _context.SaveChangesAsync();

            return commentToUpdate;
        }
    }
}
