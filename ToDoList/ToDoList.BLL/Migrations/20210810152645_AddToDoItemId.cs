﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoList.BLL.Migrations
{
    public partial class AddToDoItemId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToDoItemsDataComments_ToDoItemsData_ToDoItemId",
                table: "ToDoItemsDataComments");

            migrationBuilder.AlterColumn<int>(
                name: "ToDoItemId",
                table: "ToDoItemsDataComments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ToDoItemsDataComments_ToDoItemsData_ToDoItemId",
                table: "ToDoItemsDataComments",
                column: "ToDoItemId",
                principalTable: "ToDoItemsData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToDoItemsDataComments_ToDoItemsData_ToDoItemId",
                table: "ToDoItemsDataComments");

            migrationBuilder.AlterColumn<int>(
                name: "ToDoItemId",
                table: "ToDoItemsDataComments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ToDoItemsDataComments_ToDoItemsData_ToDoItemId",
                table: "ToDoItemsDataComments",
                column: "ToDoItemId",
                principalTable: "ToDoItemsData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
